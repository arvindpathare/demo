﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace Ecommerce.Controllers
{
    public class RegistrationController : Controller
    {
        // GET: Registration
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Header()
        {
            return View();
        }
        public ActionResult Footer()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(Ecommerce.Register user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var db = new Ecommerce.masterEntities())
                    {
                        var newUser = db.Registers.Create();
                        newUser.Name = user.Name;
                        newUser.Email = user.Email;
                        newUser.Password = user.Password;
                        newUser.Role = user.Role;

                        db.Registers.Add(newUser);
                        db.SaveChanges();
                        return View("Index");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Data is not correct");
                }
                return View(user);
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            //  return View();
        }


        private bool IsValid(string email, string password)
        {

            bool IsValid = false;

            using (var db = new Ecommerce.masterEntities())
            {
                var user = db.Registers.FirstOrDefault(u => u.Email == email);

                if (user != null)
                {
                    if (user.Password == password)
                    {

                        //Session["Email"] = user.Email.ToString();
                        //Session["Password"] = user.Password.ToString();

                        IsValid = true;
                    }
                }
            }
            return IsValid;
        }

        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]


        public ActionResult Login(Ecommerce.Register userr)
        {
            if (IsValid(userr.Email, userr.Password))
            {
                FormsAuthentication.SetAuthCookie(userr.Email, false);

                return View("Index");


            }
            else
            {
                ModelState.AddModelError("", "Login details are wrong.");
            }
            return View(userr);

        }

        public ActionResult Basket()
        {

            cartEntities context = new cartEntities();
            var sum = (from t in context.Carts select t.Total).Sum();
            ViewData["Total"] = sum;
            var s = Convert.ToDecimal(sum) + 20;
            ViewData["Sum"] = s;
            var count = (from t in context.Carts select t.Productname).Count();
            ViewData["Count"] = count;
            return View(from cart in context.Carts select cart);
        }

    }
}